<!DOCTYPE html>
<html lang="en">
  

<head>
  <meta charset="utf-8">
    <title>Customizing my Jekyll blog’s Atom feed – Abitha K Thyagarajan</title> 

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="color-scheme" content="dark light">

  <link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🪻</text></svg>">

  <link href="/styles/base_styles.css"  rel="stylesheet" />
  
  <link rel="alternate" type="application/atom+xml" title="Atom" href="https://abithakt.gitlab.io/feed.xml">

  <meta property="og:title" content="Customizing my Jekyll blog’s Atom feed – Abitha K Thyagarajan" />
  <meta property="og:site_name" content="Abitha K Thyagarajan">
  <meta property="og:type" content="article" />
  <meta property="og:url" content="https://abithakt.gitlab.io/blog/customizing-jekyll-atom-feed/" />
  <meta name="og:description" content="I&#x27;ve recently undertaken a complete revamp of this website, going through all the content, writing a new theme, and organizing things on the back-end. This is how I customized the site&#x27;s Atom feed.">
  <meta name="description" content="I&#x27;ve recently undertaken a complete revamp of this website, going through all the content, writing a new theme, and organizing things on the back-end. This is how I customized the site&#x27;s Atom feed.">
  <meta name="twitter:card" content="summary">
  
  <link rel="preload" href="/fonts/BricolageGrotesque_Latin.woff2" as="font" type="font/woff2" crossorigin>
  <link href="/styles/fonts.css" rel="stylesheet" />

</head>

<body>
  <a class="skip-to-content" href="#main">Skip to content</a>
  <header class="blog-header">
      <div>
          <div class="blog-name">
            Abitha K Thyagarajan
          </div>
      <nav>
          <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/blog">Blog</a></li>
            <li><a href="/now">Now</a></li>
            <li><a href="/feed.xml">Feed</a></li>
            <!--
            <li><a href="/colophon">Colophon</a></li>
            <li><a href="/contact">Contact me</a></li>
            -->
          </ul>
      </nav>
    </div>
  </header>


  <main aria-label="Content" id="main">
      <article>
      
<header><h1>Customizing my Jekyll blog’s Atom feed</h1>
    <p>Published: <time itemprop="datePublished" datetime="2020-06-21">June 21, 2020</time>
    <br>
    
        
        Tags: 
        
            <a href="/tags#atom">atom</a>, 
            <a href="/tags#jekyll">jekyll</a>, 
            <a href="/tags#rss">rss</a>, 
            <a href="/tags#webdev">webdev</a>
    </p>
</header>

<div class="post-content">
<div class="notice">
<p>Notice (2023-08-16): This post is now outdated, as my website no longer uses Jekyll. Nearly all the features described in this post are no longer available on this site. I am leaving this up purely for archival purposes.</p>
</div>
<p>I’ve recently undertaken a complete revamp of this website, going through all the content, writing a new theme, and organizing things on the back-end (the Jekyll site structure). In the process, I’ve learned a lot about Jekyll, CSS, Liquid templating, and web development in general. Writing my own theme allowed me to fine-tune each and every single outward-facing aspect of the website, and many of the inward-facing ones, too. This is how I fine-tuned the site’s Atom feed.</p>
<!---
I'm a heavy user of RSS[^rssatom]. I almost never subscribe to email newsletters[^newsletters]; I only consider it if a website doesn't have an RSS feed. And even then I usually decide that I don't need to follow the site's updates. :P So, of course I have high standards for my own personal website's feed! I have it in my own feed reader (I use [Newsboat](https://wiki.archlinux.org/index.php/Newsboat)), which is how I came to realize how inadequate it was.
--->
<p>I’m a heavy user of RSS<sup id="fnref:rssatom"><a href="#fn:rssatom" class="footnote">1</a></sup>. I almost never subscribe to email newsletters; I only consider it if a website doesn’t have an RSS feed. And even then I usually decide that I don’t need to follow the site’s updates. :P So, of course I have high standards for my own personal website’s feed! I have it in my own feed reader (I use <a href="https://wiki.archlinux.org/index.php/Newsboat">Newsboat</a>), which is how I came to realize how inadequate it was.</p>
<h2 id="my-problem">My problem<a class="zola-anchor" href="#my-problem" aria-label="Anchor link for: my-problem"></a>
</h2>
<p>Nearly every Jekyll website uses the <a href="https://github.com/jekyll/jekyll-feed">jekyll-feed</a> plugin. It’s a great plugin: you can set it and forget it, and it works great. By default it syndicates the entire content of a post. I don’t prefer this behaviour – I’ve put a lot of effort into this website’s theme, come and look at it! – so I set the plugin to produce a feed that only contains excerpts for each post. This is pretty close to what I wanted. However, in my preferred feed reader, each post displays like so:</p>
<figure>
<img src="/images/atom_feed_before.png" alt="Screenshot of an RSS feed entry in my terminal, using the program Newsboat. The body of the entry shows a single paragraph of text from a blog post." />
<figcaption markdown="span">
<a href="/images/atom_feed_before.png">View image at full size</a>
</figcaption>
</figure>
<p>There is no indication that there is more to this post. Sure, I have the option to view the article at the source website, but why would I do that when there’s no sign that there’s more to view than what’s shown here?</p>
<p>My requirement was simple: I want to add a line of text below the excerpt that tells the reader that there’s more where that came from.</p>
<h2 id="modification-1-customizing-the-entry-text">Modification 1: Customizing the entry text<a class="zola-anchor" href="#modification-1-customizing-the-entry-text" aria-label="Anchor link for: modification-1-customizing-the-entry-text"></a>
</h2>
<p>I had to implement jekyll-feed from scratch to do this. Well, okay, not from scratch. First I tried <a href="https://jekyllcodex.org/without-plugin/rss-feed/">this snippet from Jekyll Codex</a>. However, it doesn’t include an author name in the post entry, nor does it provide an option to syndicate only an excerpt instead of the full contents of a post.</p>
<p>Bear in mind that I’m a total noob when it comes to XML and RSS/Atom. Sure, I know how to add a feed URL to my reader, but that’s it. I don’t know anything about XML or feed structure. So I just looked at the output of jekyll-feed and added the corresponding line for including the author’s name in entries. Easy, right? But no! It threw a bunch of errors. The feed refused to load at all in Newsboat.
The few ham-fisted edits through which I attempted to rectify this led nowhere.</p>
<p>The prospect of editing the Jekyll Codex feed suddenly seemed daunting.</p>
<p>So I figured, jekyll-feed was working fine enough for my purposes. Why reinvent the wheel? I can just copy its output and add a line pointing people to the article at my blog at the end of each entry.</p>
<p>This was easier said than done. I went through the entire thing line by line, substituting Liquid variables wherever applicable. Once I did that, I was faced by a multitude of errors, and after tearing my hair out for a bit, I turned to the <a href="https://validator.w3.org/feed/check.cgi">W3C Feed Validator</a>. I implemented its suggestions one by one. I learned a lot about escaping characters for XML-safe and URI-safe output. I learned about date formats (RFC 3339, anyone?). And then I was the proud owner of a valid Atom feed!</p>
<center><a href="https://validator.w3.org/feed/check.cgi?url=https%3A//abithakt.gitlab.io/feed.xml"><img src="/images/valid-atom.png" alt="[Valid Atom 1.0]" title="Validate my Atom 1.0 feed" /></a></center>
<p>Some tricky Liquid syntax was involved. I’ve uploaded the fruits of my labour to a <a href="https://gitlab.com/snippets/1988824">GitLab snippet here</a>. There’s one block I’m particularly proud of:</p>
<!--{% raw %}-->
<pre data-lang="xml" class="language-xml z-code"><code class="language-xml" data-lang="xml"><span class="z-text z-xml"><span class="z-meta z-tag z-xml"><span class="z-punctuation z-definition z-tag z-begin z-xml">&lt;</span><span class="z-entity z-name z-tag z-localname z-xml">summary</span> <span class="z-entity z-other z-attribute-name z-localname z-xml">type</span><span class="z-punctuation z-separator z-key-value z-xml">=</span><span class="z-string z-quoted z-double z-xml"><span class="z-punctuation z-definition z-string z-begin z-xml">&quot;</span>xhtml<span class="z-punctuation z-definition z-string z-end z-xml">&quot;</span></span><span class="z-punctuation z-definition z-tag z-end z-xml">&gt;</span></span>
</span><span class="z-text z-xml">    <span class="z-meta z-tag z-xml"><span class="z-punctuation z-definition z-tag z-begin z-xml">&lt;</span><span class="z-entity z-name z-tag z-localname z-xml">div</span> <span class="z-entity z-other z-attribute-name z-localname z-xml">xmlns</span><span class="z-punctuation z-separator z-key-value z-xml">=</span><span class="z-string z-quoted z-double z-xml"><span class="z-punctuation z-definition z-string z-begin z-xml">&quot;</span>http://www.w3.org/1999/xhtml<span class="z-punctuation z-definition z-string z-end z-xml">&quot;</span></span><span class="z-punctuation z-definition z-tag z-end z-xml">&gt;</span></span>
</span><span class="z-text z-xml">        {% if post.excerpt %}
</span><span class="z-text z-xml">        {{- post.excerpt | remove: &quot;<span class="z-meta z-tag z-xml"><span class="z-punctuation z-definition z-tag z-begin z-xml">&lt;/</span><span class="z-entity z-name z-tag z-localname z-xml">p</span><span class="z-punctuation z-definition z-tag z-end z-xml">&gt;</span></span>&quot; | append: &quot; […]&quot; | markdownify -}}
</span><span class="z-text z-xml">        <span class="z-meta z-tag z-xml"><span class="z-punctuation z-definition z-tag z-begin z-xml">&lt;</span><span class="z-entity z-name z-tag z-localname z-xml">p</span><span class="z-punctuation z-definition z-tag z-end z-xml">&gt;</span></span>
</span><span class="z-text z-xml">            Read the full post at <span class="z-meta z-tag z-xml"><span class="z-punctuation z-definition z-tag z-begin z-xml">&lt;</span><span class="z-entity z-name z-tag z-localname z-xml">a</span> <span class="z-entity z-other z-attribute-name z-localname z-xml">href</span><span class="z-punctuation z-separator z-key-value z-xml">=</span><span class="z-string z-quoted z-double z-xml"><span class="z-punctuation z-definition z-string z-begin z-xml">&quot;</span>{{ post.url | prepend: site.baseurl | prepend: site.url }}<span class="z-punctuation z-definition z-string z-end z-xml">&quot;</span></span><span class="z-punctuation z-definition z-tag z-end z-xml">&gt;</span></span>{{ post.title | xml_escape }}<span class="z-meta z-tag z-xml"><span class="z-punctuation z-definition z-tag z-begin z-xml">&lt;/</span><span class="z-entity z-name z-tag z-localname z-xml">a</span><span class="z-punctuation z-definition z-tag z-end z-xml">&gt;</span></span>.
</span><span class="z-text z-xml">        <span class="z-meta z-tag z-xml"><span class="z-punctuation z-definition z-tag z-begin z-xml">&lt;/</span><span class="z-entity z-name z-tag z-localname z-xml">p</span><span class="z-punctuation z-definition z-tag z-end z-xml">&gt;</span></span>
</span><span class="z-text z-xml">        {% else %}
</span><span class="z-text z-xml">        {{ post.content | markdownify }}
</span><span class="z-text z-xml">        {% endif %}
</span><span class="z-text z-xml">    <span class="z-meta z-tag z-xml"><span class="z-punctuation z-definition z-tag z-begin z-xml">&lt;/</span><span class="z-entity z-name z-tag z-localname z-xml">div</span><span class="z-punctuation z-definition z-tag z-end z-xml">&gt;</span></span>
</span><span class="z-text z-xml"><span class="z-meta z-tag z-xml"><span class="z-punctuation z-definition z-tag z-begin z-xml">&lt;/</span><span class="z-entity z-name z-tag z-localname z-xml">summary</span><span class="z-punctuation z-definition z-tag z-end z-xml">&gt;</span></span>
</span></code></pre>
<!--{% endraw %}-->
<p><em>If</em> the post has a defined excerpt, we display that, followed by a directive to read the rest on the website. If there is no excerpt, we syndicate the entire post.</p>
<p>I wanted to have an ellipsis at the end of the excerpt, but Jekyll kept putting it on the next line, like so:</p>
<blockquote>
<p>I had a bit of a scare a couple of days ago. My laptop just came back from (hardware) repairs, and on switching it on, I was dropped into a GRUB
shell. I hadn’t updated my laptop or done anything out of the ordinary – I still don’t know why this happened. Here’s how I fixed it.</p>
<p>[…]</p>
</blockquote>
<p>That is, just using the <code>append</code> Liquid filter wasn’t cutting it. I used <code>strip</code> to strip a newline. No change. I suspected the presence of a closing paragraph tag to be the culprit, so I tried stripping the HTML tags from the excerpt (not a good idea, in retrospect). Still no change. Then, after some trial and error, I came up with this line:</p>
<!--{% raw %}-->
<pre class="z-code"><code><span class="z-text z-plain">{{- post.excerpt | remove: &quot;&lt;/p&gt;&quot; | append: &quot; […]&quot; | markdownify -}}
</span></code></pre>
<!--{% endraw %}-->
<p>This is clever because <code>&lt;/p&gt;</code> is <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/p">an optional tag</a>! Once we remove it, <code>markdownify</code> will make sure it gets closed. Even if it’s not closed, there’s no harm done. This approach retains all the HTML in the excerpt as well.</p>
<figure>
<img src="/images/atom_feed_after.png" alt="Screenshot of an RSS feed entry in my terminal, using the program Newsboat. The body of the entry shows a single paragraph of text from a blog post truncated by an ellipsis, followed by a line directing the reader to read the entire post." />
<figcaption markdown="span">
<a href="/images/atom_feed_after.png">View image at full size</a>
</figcaption>
</figure>
<p>Voilà, our desired result!</p>
<!-- And I learned how ugly my feed looked after I was done with it. -->
<h2 id="modification-2-feed-styling">Modification 2: Feed styling<a class="zola-anchor" href="#modification-2-feed-styling" aria-label="Anchor link for: modification-2-feed-styling"></a>
</h2>
<p>After I was done creating my brand new Atom feed, Jekyll (or maybe my browser?) chose to render it like a normal HTML page.  I suspect the use of <code>markdownify</code>. In any case, it was ugly:</p>
<figure>
<img src="/images/atom_feed_browser_before.png" alt="Screenshot of the feed in my browser, appearing as a wall of text." />
<figcaption markdown="span">
<a href="/images/atom_feed_browser_before.png">View image at full size</a>
</figcaption>
</figure>
<p>I looked up styling XML. I learned that you can use an <a href="https://www.bennadel.com/blog/3770-using-xslt-and-xml-transformations-to-style-my-blogs-rss-feed-as-an-html-page.htm#main-content">XSLT</a> file, but I wasn’t interested in playing with (to me) a brand new syntax. You can achieve more limited styling with plain old CSS. And that’s what I did! Meet my <a href="/feed.xml">gorgeous new feed</a>:</p>
<figure>
<img src="/images/atom_feed_browser_after.png" alt="Screenshot of the feed in my browser, looking like a normal webpage with clearly defined headings and post entries." />
<figcaption markdown="span">
<a href="/images/atom_feed_browser_after.png">View image at full size</a>
</figcaption>
</figure>
<p>All I needed to do was insert a line after the XML version:</p>
<pre data-lang="xml" class="language-xml z-code"><code class="language-xml" data-lang="xml"><span class="z-text z-xml"><span class="z-meta z-tag z-preprocessor z-xml"><span class="z-punctuation z-definition z-tag z-begin z-xml">&lt;?</span><span class="z-entity z-name z-tag z-xml">xml-stylesheet</span> <span class="z-entity z-other z-attribute-name z-localname z-xml">type</span><span class="z-punctuation z-separator z-key-value z-xml">=</span><span class="z-string z-quoted z-double z-xml"><span class="z-punctuation z-definition z-string z-begin z-xml">&quot;</span>text/css<span class="z-punctuation z-definition z-string z-end z-xml">&quot;</span></span> <span class="z-entity z-other z-attribute-name z-localname z-xml">href</span><span class="z-punctuation z-separator z-key-value z-xml">=</span><span class="z-string z-quoted z-double z-xml"><span class="z-punctuation z-definition z-string z-begin z-xml">&quot;</span>/rss-style.css<span class="z-punctuation z-definition z-string z-end z-xml">&quot;</span></span> <span class="z-punctuation z-definition z-tag z-end z-xml">?&gt;</span></span>
</span></code></pre>
<p><a href="/feed.xml">Check out the feed here</a>, and add it to your RSS reader :) <span class="end"></span></p>
<hr />
<div class="footnotes">
  <ol>
    <li id="fn:rssatom">
      <p>I use the terms RSS and Atom interchangeably throughout the post. For my ends, they’re interchangeable; but it’s worth noting that <a href="https://en.wikipedia.org/wiki/Atom_(Web_standard)#Atom_compared_to_RSS_2.0">they’re different</a>.&nbsp;<a href="#fnref:rssatom" class="reversefootnote">↩</a></p>
    </li>
  </ol>
</div>

</div>

<footer>
    <hr>
    <p>Thanks for reading! Comments, questions, and suggestions are welcome via <a href="mailto:&#097;&#098;&#105;&#116;&#104;&#097;&#064;&#112;&#109;&#046;&#109;&#101;?subject=Comment on 'Customizing my Jekyll blog’s Atom feed'">email</a>.</p>
    
    <nav>
        <span>
            
            <a class="previous" href="&#x2F;blog&#x2F;fresh-new-look-jekyll&#x2F;">« A fresh new look, and what I’ve learned in the process</a>
            
        </span>
        
        <span>
            <a class="next" href="&#x2F;blog&#x2F;arch-linux-drops-into-grub-shell-on-boot&#x2F;">Roadblocks: Arch Linux drops into a GRUB shell on boot »</a>
        </span>
        
    </nav> 
    

</footer>

      </article>
 </main>


  <footer class="blog-footer" id="footer">
      <p class="print-thank-you"></p>
      <p>&copy;&nbsp;2017&ndash;2024 Abitha K Thyagarajan. All rights reserved.  <a href="#">Back to top.</a></p>
      
    </footer>
</body>

</html>

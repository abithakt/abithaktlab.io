<!DOCTYPE html>
<html lang="en">
  

<head>
  <meta charset="utf-8">
    <title>Simple LaTeX wordcount – Abitha K Thyagarajan</title> 

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="color-scheme" content="dark light">

  <link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🪻</text></svg>">

  <link href="/styles/base_styles.css"  rel="stylesheet" />
  
  <link rel="alternate" type="application/atom+xml" title="Atom" href="https://abithakt.gitlab.io/feed.xml">

  <meta property="og:title" content="Simple LaTeX wordcount – Abitha K Thyagarajan" />
  <meta property="og:site_name" content="Abitha K Thyagarajan">
  <meta property="og:type" content="article" />
  <meta property="og:url" content="https://abithakt.gitlab.io/blog/simple-latex-wordcount/" />
  <meta name="og:description" content="There are a lot of ways to accomplish this, but I wanted to highlight a simple CLI approach I&#x27;ve been using…">
  <meta name="description" content="There are a lot of ways to accomplish this, but I wanted to highlight a simple CLI approach I&#x27;ve been using…">
  <meta name="twitter:card" content="summary">
  
  <link rel="preload" href="/fonts/BricolageGrotesque_Latin.woff2" as="font" type="font/woff2" crossorigin>
  <link href="/styles/fonts.css" rel="stylesheet" />

</head>

<body>
  <a class="skip-to-content" href="#main">Skip to content</a>
  <header class="blog-header">
      <div>
          <div class="blog-name">
            Abitha K Thyagarajan
          </div>
      <nav>
          <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/blog">Blog</a></li>
            <li><a href="/now">Now</a></li>
            <li><a href="/feed.xml">Feed</a></li>
            <!--
            <li><a href="/colophon">Colophon</a></li>
            <li><a href="/contact">Contact me</a></li>
            -->
          </ul>
      </nav>
    </div>
  </header>


  <main aria-label="Content" id="main">
      <article>
      
<header><h1>Simple LaTeX wordcount</h1>
    <p>Published: <time itemprop="datePublished" datetime="2019-12-20">December 20, 2019</time>
    <br>
    
        
        Tags: 
        
            <a href="/tags#latex">latex</a>, 
            <a href="/tags#linux">linux</a>, 
            <a href="/tags#shell">shell</a>
    </p>
</header>

<div class="post-content">
<p>I like using <code>wc</code> to keep track of how much I’ve written and to keep track of my
wordcounts. However, <code>wc</code> is not the best choice to find out how many words
you’ve written in a LaTeX document – it’s better to use something like
<a href="https://tex.stackexchange.com/questions/534/is-there-any-way-to-do-a-correct-word-count-of-a-latex-document"><code>texcount</code></a>.</p>
<p>There are a lot of ways to accomplish this, but I wanted to highlight a simple
CLI approach I’ve been using:</p>
<pre class="z-code"><code><span class="z-text z-plain">#!/bin/bash
</span><span class="z-text z-plain">
</span><span class="z-text z-plain">texcount {$1} -total | awk &#39;{print $NF}&#39; | tail -n 8 | sed &#39;:a;/[0-9]$/{N;s/\n/+/;ba}&#39; | sed &#39;s/$/0/g&#39; | qalc | head -n 3 | tail -n 1 | awk &#39;{print $NF}&#39;
</span></code></pre>
<p>Let’s look at what this does, shall we?</p>
<ul>
<li><code>texcount</code> is the main character here. Its typical output is a bit long-winded
for my tastes:</li>
</ul>
<pre class="z-code"><code><span class="z-text z-plain">File: latexworkshop.tex
</span><span class="z-text z-plain">Encoding: utf8
</span><span class="z-text z-plain">Words in text: 210
</span><span class="z-text z-plain">Words in headers: 3
</span><span class="z-text z-plain">Words outside text (captions, etc.): 0
</span><span class="z-text z-plain">Number of headers: 1
</span><span class="z-text z-plain">Number of floats/tables/figures: 0
</span><span class="z-text z-plain">Number of math inlines: 0
</span><span class="z-text z-plain">Number of math displayed: 0
</span><span class="z-text z-plain">Subcounts:
</span><span class="z-text z-plain">  text+headers+captions (#headers/#floats/#inlines/#displayed)
</span><span class="z-text z-plain">  1+0+0 (0/0/0/0) _top_
</span><span class="z-text z-plain">  209+3+0 (1/0/0/0) Section: LaTeX workshop proposal}\label{latex-workshop-proposal}
</span></code></pre>
<ul>
<li>This is a script, so <code>{$1}</code> gets the first command-line argument;
<code>#!/bin/bash</code> is the hashbang, and tells the shell which program it should use
to execute the script.</li>
<li>I want my output to only have the number of words, so let’s use <code>-total</code> with
<code>texcount</code>.</li>
<li><code>awk '{print $NF}'</code> gets the last word on each line – which is a number.</li>
<li><code>tail -n 8</code> gets the last 8 lines – skipping the filename.</li>
<li><code>sed ':a;/[0-9]$/{N;s/\n/+/;ba}'</code> puts all the numbers on the same line, with
a <code>+</code> between each pair of numbers.</li>
<li><code>sed 's/$/0/g'</code> appends a <code>0</code> to the end (to take care of the trailing <code>+</code>).</li>
<li><code>qalc</code> is my preferred CLI caclulator. We feed the equation obtained in the
previous step into <code>qalc</code> and get the sum.</li>
<li><code>head -n 3</code> and <code>tail -n 1</code> isolate the line with the sum. Come to think of
it, I think you could <code>grep</code> for the <code>=</code> sign.</li>
<li>Finally, <code>awk '{print $NF}'</code> gets the last word – which is the total
wordcount. <span class="end"></span></li>
</ul>

</div>

<footer>
    <hr>
    <p>Thanks for reading! Comments, questions, and suggestions are welcome via <a href="mailto:&#097;&#098;&#105;&#116;&#104;&#097;&#064;&#112;&#109;&#046;&#109;&#101;?subject=Comment on 'Simple LaTeX wordcount'">email</a>.</p>
    
    <nav>
        <span>
            
            <a class="previous" href="&#x2F;blog&#x2F;choosing-a-random-essay-topic&#x2F;">« Choosing a random essay topic</a>
            
        </span>
        
        <span>
            <a class="next" href="&#x2F;blog&#x2F;dynamic-motd-on-arch&#x2F;">Dynamic MOTD on Arch Linux »</a>
        </span>
        
    </nav> 
    

</footer>

      </article>
 </main>


  <footer class="blog-footer" id="footer">
      <p class="print-thank-you"></p>
      <p>&copy;&nbsp;2017&ndash;2024 Abitha K Thyagarajan. All rights reserved.  <a href="#">Back to top.</a></p>
      
    </footer>
</body>

</html>
